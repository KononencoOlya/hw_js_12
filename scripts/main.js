const btn = document.querySelector('.btn-wrapper');
const btnItem = document.querySelectorAll('.btn')
btn.addEventListener('keydown', (ev) => {
  
  let keyName = ev.code;
  const button = document.querySelector('[data-key="' + keyName + '"]');
  button.classList.add('active')
  btnItem.forEach(el => {
    if(el != button) {
      el.classList.remove('active')
    }
  })

})